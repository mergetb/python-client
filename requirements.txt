setuptools==40.4.3
requests==2.20.0
ipython==7.1.1
PyYAML==3.13
flake8==3.5.0
pylint==2.3.0
python-dateutil
