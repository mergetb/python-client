mergetb.lib
===========

.. automodule:: mergetb.lib

   
   
   .. rubric:: Functions

   .. autosummary::
   
      accept_realization
      account_delete
      account_init
      add_pubkey
      delete_experiment
      delete_project
      delete_pubkey
      delete_realization
      delete_site
      dematerialize
      destroy_xdc
      do_delete
      do_get
      do_post
      do_post_file
      do_put
      do_put_file
      experiment_history
      experiment_info
      fetch_fs_token
      fetch_web_token
      get_materialization
      get_realization
      health
      init
      list_experiments
      list_materializations
      list_projects
      list_pubkeys
      list_realizations
      list_sites
      list_users
      list_xdcs
      materialize
      new_experiment
      new_project
      new_site
      project_add_member
      project_delete_member
      project_experiments
      project_info
      project_member
      project_members
      project_update_member
      push_experiment
      push_experiment_file
      realize
      reject_realization
      set_api
      set_token
      show
      spawn_xdc
      update_experiment
      update_project
      update_user
      user_delete
      user_info
      user_validate
      user_vtoken
      xdc_token
   
   

   
   
   

   
   
   