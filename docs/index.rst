.. mergetb documentation master file, created by
   sphinx-quickstart on Thu Feb 28 22:41:46 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MergeTB API Docs
================

.. autosummary::
   :toctree: stubs/
   
   mergetb.lib

